TP =

vm-make:
	qmm cmd -i -u -s "cd /qmm/vitis; make clean; make"

vm-eliminate:
	qmm eliminate

vm-upload:
	qmm upload --from . --to /qmm

vm-connect:
	qmm connect

vm-test-cli:
	qmm cmd -i -u --rm -c test-cli

vm-test-fs:
	qmm cmd -u -i --rm -c test-fs

TEST_FS_P_CMD = "cd /qmm/vitis; make clean; make; make test-fs-p TP=$(TP)"

# example of usage: make vm-test-fs-p TP=rename
vm-test-fs-p:
	qmm cmd -u -i --rm -s $(TEST_FS_P_CMD)

test-cli:
	pytest-3 tests/cli/test_mkfs.py -v -s
	pytest-3 tests/cli/test_mount.py -v -s
	rm -rf tests/__pycache__   tests/.pytest_cache
	rm -rf tests/*/__pycache__ tests/*/.pytest_cache

test-fs:
	pytest-3 tests/operations/test_001_initialize/   -v -s
	pytest-3 tests/operations/test_002_getattr/      -v -s
	pytest-3 tests/operations/test_003_mkdir/        -v -s
	pytest-3 tests/operations/test_004_mknod/        -v -s
	pytest-3 tests/operations/test_005_create/       -v -s
	pytest-3 tests/operations/test_006_symlink/      -v -s
	pytest-3 tests/operations/test_007_readlink/     -v -s
	pytest-3 tests/operations/test_008_link/         -v -s
	pytest-3 tests/operations/test_009_access/       -v -s
	pytest-3 tests/operations/test_010_rename/       -v -s
	pytest-3 tests/operations/test_011_rmdir/        -v -s
	pytest-3 tests/operations/test_012_unlink/       -v -s
	pytest-3 tests/operations/test_013_open/         -v -s
	pytest-3 tests/operations/test_014_write/        -v -s
	pytest-3 tests/operations/test_015_read/         -v -s
	pytest-3 tests/operations/test_016_lseek/        -v -s
	pytest-3 tests/operations/test_017_truncate/     -v -s
	pytest-3 tests/operations/test_018_release/      -v -s
	pytest-3 tests/operations/test_019_opendir/      -v -s
	pytest-3 tests/operations/test_020_readdir/      -v -s
	pytest-3 tests/operations/test_021_releasedir/   -v -s
	pytest-3 tests/operations/test_022_statfs/       -v -s
	pytest-3 tests/operations/test_023_listxattr/    -v -s
	pytest-3 tests/operations/test_024_getxattr/     -v -s
	pytest-3 tests/operations/test_025_setxattr/     -v -s
	pytest-3 tests/operations/test_026_removexattr/  -v -s
	pytest-3 tests/operations/test_pseudocategories/ -v -s
	rm -rf tests/__pycache__   tests/.pytest_cache
	rm -rf tests/*/__pycache__ tests/*/.pytest_cache

test-fs-p:
	pytest-3 tests/operations/test_*_$(TP)/ -v -s
	rm -rf tests/__pycache__   tests/.pytest_cache
	rm -rf tests/*/__pycache__ tests/*/.pytest_cache
