#!/bin/bash
set -eu -o pipefail
set -x

IM="/tmp/ds.fs"
MP="/mnt/vitis"
VTS="build/vitis"
PRIMARY_MP="/etc/vitis/loop81"

if [[ "$#" -gt 0 && "$1" == "remove" ]]; then
    $VTS umount -i "$IM"
    sleep 2
    rm -f "$IM"
else
    mkdir -p "$MP"
    $VTS mkfs -i "$IM" -s 5G
    $VTS mount -i "$IM" -m "$MP" -f
fi
