#!/usr/bin/env python3

import subprocess
import sys

sys.path.append('..')
from common import get_output
from conftest import fs_environment, make_cmd_with_conf


def test_case01(fs_environment):
    '''
    Simple usage of "vitis show -d <category>"
    '''
    cmd = make_cmd_with_conf('show -d Text')
    import os
    output = subprocess.check_output(cmd).decode('utf-8')
    expected = '''[1] Fiction/
[2] april.txt
[3] march.txt
[4] may.txt
'''
    assert output == expected


def test_case02(fs_environment):
    '''
    Simple usage of "vitis show -e <expression>"
    '''
    cmd = make_cmd_with_conf('show -e Text')
    output = subprocess.check_output(cmd).decode('utf-8')
    expected = '''[1] april.txt
[2] bigtext.txt
[3] march.txt
[4] may.txt
[5] yeats.txt
'''
    assert output == expected


def test_case03(fs_environment):
    '''
    Non-existent category in "vitis show -d <category>"
    '''
    cmd = make_cmd_with_conf('show -d Unknown')
    exit_code, stdout_content, stderr_content = get_output(cmd)
    expected = 'Unknown: this category doesn\'t exist.\n'
    assert exit_code != 0
    assert stdout_content == ''
    assert stderr_content == expected


def test_case04(fs_environment):
    '''
    Non-existent category in "vitis show -e <expression>"
    '''
    cmd = make_cmd_with_conf('show -e Unknown')
    exit_code, stdout_content, stderr_content = get_output(cmd)
    expected = 'The expression contains non-existent categories.\n'
    assert exit_code != 0
    assert stdout_content == ''
    assert stderr_content == expected


def test_case05(fs_environment):
    '''
    Complicated expression for "vitis show -e <expression>"
    '''
    cmd = make_cmd_with_conf('show -e Text % Poetry @ { Spring \\ Text }')
    output = subprocess.check_output(cmd).decode('utf-8')
    expected = '[1] spring_file\n[2] yeats.txt\n'
    assert output == expected


def test_case06(fs_environment):
    '''
    Displaying categories: "vitis show -e <expression> --categories"
    '''
    cmd = make_cmd_with_conf('show -e Poetry u: Text u: Spring --categories')
    output = subprocess.check_output(cmd).decode('utf-8')
    expected = '''[1] april.txt
"Spring" "Text"
[2] bigtext.txt
"Text/Fiction"
[3] march.txt
"Spring" "Text"
[4] may.txt
"Spring" "Text"
[5] spring_file
"Spring"
[6] yeats.txt
"Poetry" "Text/Fiction"
'''
    assert output == expected


def test_case07(fs_environment):
    '''
    Displaying categories: "vitis show -d <category> --categories"
    '''
    cmd = make_cmd_with_conf('show -d Spring --categories')
    output = subprocess.check_output(cmd).decode('utf-8')
    expected = '''[1] april.txt
"Spring" "Text"
[2] march.txt
"Spring" "Text"
[3] may.txt
"Spring" "Text"
[4] spring_file
"Spring"
'''
    assert output == expected


def test_case08(fs_environment):
    '''
    Displaying details: "vitis show -e <expression> --details"
    '''
    cmd = make_cmd_with_conf('show -e Spring --details')
    output = subprocess.check_output(cmd).decode('utf-8')
    expected = '''[1] april.txt
Modified: 2019-04-16 00:00:01	Permissions: rw-r--r--
Accessed: 2019-06-16 02:00:00	Size: 30 B
[2] march.txt
Modified: 2019-03-16 10:07:00	Permissions: rw-r--r--
Accessed: 2019-06-16 03:00:00	Size: 31 B
[3] may.txt
Modified: 2019-05-16 03:50:09	Permissions: rw-r--r--
Accessed: 2019-06-16 01:00:00	Size: 31 B
[4] spring_file
Modified: 1970-01-01 00:00:00	Permissions: rw-r--r--
Accessed: 1985-02-02 00:00:00	Size: 0 B
'''
    assert output == expected
