#!/usr/bin/env python3

import os
import sys
import subprocess
from pathlib import Path

import pytest

sys.path.append('..')
from common import get_output
from conftest import make_cmd


def clean(image_file: str):
    os.remove(image_file)


def test_case01():
    """Non-default parameters for 'vitis mkfs'."""
    cmd = make_cmd('mkfs -i image.fs -s 12G')
    subprocess.check_output(cmd)
    image_exists = os.path.exists('image.fs')
    stat: os.stat_result = os.stat('image.fs')
    clean('image.fs')

    assert image_exists
    assert stat.st_size == 12*(2**30)  # 12 GiB


def test_case02():
    """Default size parameter for 'vitis mkfs'."""
    cmd = make_cmd('mkfs -i vitis.img')
    subprocess.check_output(cmd)
    image_exists = os.path.exists('vitis.img')
    stat: os.stat_result = os.stat('vitis.img')
    clean('vitis.img')

    assert image_exists
    assert stat.st_size == 10*(2**30)  # 10 GiB


# 0, 20 MiB and 64 Mib minus 1 byte
@pytest.mark.parametrize('size', ['0', '20M', '67108863'])
def test_small_size(size: str):
    """Small values of size parameter for 'vitis mkfs'."""
    assert not os.path.exists('vitis.img')

    cmd = make_cmd(f'mkfs -i vitis.img -s {size}')
    exit_code, stdout, stderr = get_output(cmd)

    assert exit_code == 2  # code of wrong usage
    assert stderr == 'Minimal size: 64 MiB.\n'
    assert not os.path.exists('vitis.img')


@pytest.mark.parametrize('size', ['67108864', '67108865', '1G'])
def test_normal_size(size: str):
    """Normal values of size parameter for 'vitis mkfs'."""
    assert not os.path.exists('vitis.img')

    cmd = make_cmd(f'mkfs -i vitis.img -s {size}')
    exit_code, stdout, stderr = get_output(cmd)

    assert exit_code == 0
    assert os.path.exists('vitis.img')
    clean('vitis.img')


def test_very_big_size():
    """Unavialable size for prepared VM."""
    assert not os.path.exists('vitis.img')

    cmd = make_cmd(f'mkfs -i vitis.img -s 1T')
    exit_code, stdout, stderr = get_output(cmd)

    assert exit_code == 1  # code of common error
    assert stderr == 'Not enough disk space.\n'
    assert not os.path.exists('vitis.img')
