#!/usr/bin/env python3

import os
import subprocess
import time
import shutil

import pytest

from common import mkdirs, write_to_file, touch
import common


IM = '/tmp/ds.fs'
MP = '/mnt/vitis'
VTS = 'build/vitis'
DS = '/etc/vitis/loop81'


def mount_vitis():
    cmd = [VTS, 'mount', IM, MP]
    subprocess.Popen(cmd, shell=False)
    time.sleep(0.1)


yeats = '''William Butler Yeats
He Wishes for the Cloths of Heaven

Had I the heaven's embroidered cloths,
Enwrought with golden and silver light,
The blue and the dim of the dark cloths
Of night and light and the half-light,
I would spread the cloths under your feet:
But I being poor have only my dreams;
I have spread my dreams under your feet;
Tread softly, because you tread on my dreams.
'''

lorem = '''Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit
esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'''

c10bytes = '0123456789'
c16bytes = '0123456789ABCDEF'
c30bytes = c10bytes * 3
c31bytes = c30bytes + '0'

FILESDIR = '/tmp/Files'

FILE1 = f'{FILESDIR}/file1.txt'
FILE2 = f'{FILESDIR}/file2.txt'
FILE3 = f'{FILESDIR}/file3.txt'
FILE4 = f'{FILESDIR}/file4.txt'
FILE5 = f'{FILESDIR}/file5.txt'


def create_file_env():
    os.mkdir(FILESDIR)
    touch(FILE1)
    touch(FILE2)
    touch(FILE3)
    touch(FILE4)
    touch(FILE5)


def create_category_env():
    test_root = os.getcwd()
    os.chdir(MP)
    mkdirs(['Poetry', 'Spring', 'Text/Fiction', 'Moon'])
    write_to_file('Poetry/yeats.txt', yeats)
    write_to_file('Text/Fiction/bigtext.txt', lorem)
    os.link('Poetry/yeats.txt', 'Text/Fiction/yeats.txt')
    write_to_file('Text/march.txt', c31bytes)
    write_to_file('Text/april.txt', c30bytes)
    write_to_file('Text/may.txt', c31bytes)
    os.link('Text/march.txt', 'Spring/march.txt')
    os.link('Text/april.txt', 'Spring/april.txt')
    os.link('Text/may.txt', 'Spring/may.txt')
    write_to_file('Spring/spring_file', '')
    touch('Spring/spring_file', '1985-02-02 00:00:00', '1970-01-01 00:00:00')
    touch('Spring/march.txt',   '2019-06-16 03:00:00', '2019-03-16 10:07:00')
    touch('Spring/april.txt',   '2019-06-16 02:00:00', '2019-04-16 00:00:01')
    touch('Spring/may.txt',     '2019-06-16 01:00:00', '2019-05-16 03:50:09')
    os.chdir(test_root)


def get_st_dev(path: str) -> int:
    try:
        return os.stat(path).st_dev
    except:
        return -1


def _setup_env(autocategories: str = ''):
    if not os.path.exists(MP):
        os.mkdir(MP)
    os.chmod(MP, common.MODE_777)
    cmd_mkfs = make_cmd(f'mkfs -i {IM} -s 5G')
    subprocess.Popen(cmd_mkfs).wait()
    os.umask(0o0002)  # Ubuntu style umask
    if autocategories == '':
        cmd_mount = make_cmd(f'mount -i {IM} -m {MP}') #+ ['']
    else:
        cmd_mount = make_cmd(f'mount -i {IM} -m {MP} -a {autocategories}')
    subprocess.Popen(cmd_mount)
    base_dev = os.stat('/mnt').st_dev
    vitis_dev = get_st_dev(MP)
    i: int = 0
    while vitis_dev == base_dev or vitis_dev == -1:
        time.sleep(0.01)
        vitis_dev = get_st_dev(MP)
        i += 1
        if i > 200:
            raise Exception('Failed to mount.')
    time.sleep(0.1)


def _teardown_env():
    os.sync()
    time.sleep(0.1)
    subprocess.Popen([VTS, 'umount', '-m', MP]).wait()
    os.remove(IM)

    base_dev = os.stat('/mnt').st_dev
    vitis_dev = get_st_dev(MP)
    i: int = 0
    while vitis_dev != base_dev or vitis_dev == -1:
        time.sleep(0.01)
        vitis_dev = get_st_dev(MP)
        i += 1
        if i > 200:
            raise Exception('Failed to umount.')
    time.sleep(0.1)


@pytest.fixture()
def fs_empty_environment():
    # setup
    _setup_env()

    # test break
    yield

    # teardown
    _teardown_env()


@pytest.fixture()
def fs_environment():
    # setup
    _setup_env()
    create_category_env()

    # test break
    yield

    # teardown
    _teardown_env()


@pytest.fixture()
def fs_extended_environment():
    # setup
    _setup_env()
    create_category_env()
    create_file_env()

    # test break
    yield

    # teardown
    _teardown_env()
    shutil.rmtree(FILESDIR)


def run_c_test(case_number: int, test_dir: str):
    pwd: str = os.getcwd()
    os.chdir(test_dir)
    try:
        cmd: list = f'gcc tests.c -o tests'.split()
        subprocess.Popen(cmd).wait()
        process_object = subprocess.Popen(['./tests', f'{case_number}'])
        process_object.wait()
        os.remove('tests')
        assert process_object.returncode == 0, process_object.stderr
    finally:
        os.chdir(pwd)


def make_cmd_with_conf(cmd: str):
    """
    Makes test vitis command by string with arguments
    with the special test configuration.
    """
    return [VTS] + cmd.split() + ['--conf', 'tests/cli.config']


def make_cmd(cmd: str):
    """Makes test vitis command by string with arguments."""
    return [VTS] + cmd.split()
