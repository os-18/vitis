#pragma once

#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/xattr.h>
#include <fcntl.h>

#define MP "/mnt/vitis/"
#define DS "/etc/vitis/loop81/"

int read_text(const char* filepath, char* buf, int size) {
    int len = size;
    memset(buf, '\0', len);
    char* ptr = buf;
    int fd = open(filepath, O_RDONLY);
    if (fd == -1) {
        return fd;
    }
    int ret;
    while (len != 0 && (ret = read(fd, ptr, len)) != 0 && len < size) {
        if (ret == -1) {
            if (errno == EINTR) {
                continue;
            }
            close(fd);
            return ret;
        }
        len -= ret;
        ptr += ret;
    }
    close(fd);
    return size - len;
}


int equal_stats(
    const struct stat* st1,
    const struct stat* st2,
    int check_nlink
) {
    if (check_nlink && st1->st_nlink != st2->st_nlink) {
        return 0;
    }
    return
        st1->st_dev == st2->st_dev &&
        st1->st_ino == st2->st_ino &&
        st1->st_mode == st2->st_mode &&
        st1->st_uid == st2->st_uid &&
        st1->st_gid == st2->st_gid &&
        st1->st_rdev == st2->st_rdev &&
        st1->st_size == st2->st_size &&
        st1->st_blksize == st2->st_blksize &&
        st1->st_blocks == st2->st_blocks;
}


void strcmp_assert(const char* line1, const char* line2) {
    if (0 == strcmp(line1, line2)) return;  // all right
    fprintf(stderr, "Strings don't match:\n");
    fprintf(stderr, "\n--Line 1--\n");
    fprintf(stderr, line1);
    fprintf(stderr, "\n--Line 2--\n");
    fprintf(stderr, line2);
    fprintf(stderr, "\n--End--\n");
    assert(0);
}


void check_next_line(FILE* fp, const char* expected_line) {
    size_t len = 0;
    ssize_t rlen = 0;
    char* line = NULL;
    rlen = getline(&line, &len, fp);
    assert(rlen != -1);
    strcmp_assert(line, expected_line);
    free(line);  // after getline()
}


void show_next_line(FILE* fp) {
    size_t len = 0;
    ssize_t rlen = 0;
    char* line = NULL;
    rlen = getline(&line, &len, fp);
    assert(rlen != -1);
    fprintf(stdout, "\n__Read line__\n");
    fprintf(stdout, line);
    fprintf(stdout, "--End of line--\n");
    free(line);  // after getline()
}


int starts_with(const char* str, const char* substr) {
    size_t str_len = strlen(str);
    size_t substr_len = strlen(substr);
    if (substr_len > str_len) {
        return 0;
    }
    for (size_t i = 0; i < substr_len; i++) {
        if (str[i] != substr[i]) {
            return 0;
        }
    }
    return 1;
}


int ends_with(const char* str, const char* substr) {
    size_t str_len = strlen(str);
    size_t substr_len = strlen(substr);
    if (substr_len > str_len) {
        return 0;
    }
    for (size_t i = 0; i < substr_len; i++) {
        if (str[str_len - substr_len + i] != substr[i]) {
            return 0;
        }
    }
    return 1;
}


void assertZero4KiB(const char* filepath) {
    struct stat st;
    assert(0 == stat(filepath, &st));
    assert(st.st_size == 4096);
    char buffer[4096];
    read_text(filepath, buffer, 4096);
    for (size_t i = 0; i < 4096; i++) {
        assert(buffer[i] == 0);
    }
}


int check_attr(
    const char* attrs_file,
    const char* attr_name,
    const char* attr_value
) {
    FILE* fp = fopen(attrs_file, "r");
    assert(fp != NULL);
    char expected_line[1024] = {0};
    sprintf(expected_line, "\"%s\",\"%s\"\n", attr_name, attr_value);

    size_t len = 0;
    ssize_t rlen = 0;
    char* line = NULL;
    int line_found = 0;  // status
    while (1) {  // read file
        rlen = getline(&line, &len, fp);
        if (rlen == -1) {  // end of file
            break;
        }
        if (0 == strcmp(line, expected_line)) {
            line_found = 1;
        }
        free(line);  // after getline()
        line = NULL;
        if (line_found) {
            break;
        }
    }
    return line_found;
}


void check_attrs(const char* attrs_file, char filetype, const char* name) {
    FILE* fp = fopen(attrs_file, "r");
    assert(fp != NULL);
    const char* expected = NULL;
    // header
    expected = R"("key","value")""\n";
    check_next_line(fp, expected);
    // vitis.canon_name
    char cn_expected[64] = {0};
    sprintf(cn_expected, "\"vitis.canon_name\",\"%s\"\n", name);
    check_next_line(fp, cn_expected);
    // vitis.filetype
    char ft_expected[64] = {0};
    sprintf(ft_expected, "\"vitis.filetype\",\"%c\"\n", filetype);
    check_next_line(fp, ft_expected);
    // vitis.filemode
    expected = R"("vitis.filemode","000111111101")""\n";  // if umask is 0002
    check_next_line(fp, expected);
    // vitis.userid
    expected = R"("vitis.userid","0")""\n";  // root user is 0
    check_next_line(fp, expected);
    // vitis.groupid
    expected = R"("vitis.groupid","0")""\n";  // root group is 0
    check_next_line(fp, expected);
    // vitis.stat.nlink
    expected = R"("vitis.stat.nlink","2")""\n";
    check_next_line(fp, expected);
    char* line = NULL;
    size_t len = 0;
    ssize_t rlen;
    rlen = getline(&line, &len, fp);
    assert(starts_with(line, "\"vitis.stat.btime\",\""));
    free(line);
    line = NULL;
    rlen = getline(&line, &len, fp);
    assert(starts_with(line, "\"vitis.stat.mtime\",\""));
    free(line);
    line = NULL;
    rlen = getline(&line, &len, fp);
    assert(starts_with(line, "\"vitis.stat.atime\",\""));
    free(line);
    line = NULL;
    rlen = getline(&line, &len, fp);
    assert(starts_with(line, "\"vitis.stat.ctime\",\""));
    free(line);
    line = NULL;
    rlen = getline(&line, &len, fp);
    assert(rlen == -1);  // end of file
    fclose(fp);
}


void check_dds_file(
    const char* dds_file,
    ino_t parent_ino,
    const char* name,
    size_t line_id,
    int nrecords
) {
    const size_t LINE_LENGTH = 256;
    const size_t INO_OFFSET = 248;
    const size_t SF_SIZE = 3;
    const size_t SPEC_FIELD_POS = INO_OFFSET - SF_SIZE;

    FILE* categories_fp = fopen(dds_file, "r");
    assert(categories_fp != NULL);
    char byte_line[LINE_LENGTH];
    size_t ret = 0;
    for (size_t i = 0; i < line_id; i++) {  // find line
        ret = fread(byte_line, 1, LINE_LENGTH, categories_fp);
        assert(ret == LINE_LENGTH);
        if (i == 0) {
            // special field
            int last_elem_index = nrecords - 1;
            int cmp_res = memcmp(
                byte_line + SPEC_FIELD_POS, &last_elem_index, SF_SIZE
            );
            if (0 != cmp_res) {
                assert(0);
            }
        }
    }
    const size_t NAME_LEN = strlen(name);
    if (0 != memcmp(byte_line, name, NAME_LEN)) {
        fprintf(stderr, "memcmp: %s\n", byte_line);
        assert(0);
    }

    // zeros after name
    const size_t ZBT_LEN = INO_OFFSET - NAME_LEN - SF_SIZE;
    char zeros_before_tail[ZBT_LEN];
    memset(zeros_before_tail, 0, ZBT_LEN);
    char* pos_zbt = byte_line + NAME_LEN;
    if (0 != memcmp(pos_zbt, zeros_before_tail, ZBT_LEN)) {
        assert(0);
    }

    // ino field
    const size_t INO_SIZE = 8;
    if (0 != memcmp(byte_line + INO_OFFSET, &parent_ino, INO_SIZE)) {
        assert(0);
    }

    // only zeros in file end

    for (ssize_t i = 0; i < nrecords - line_id; i++) {  // skip unneeded
        ret = fread(byte_line, 1, LINE_LENGTH, categories_fp);
        assert(ret == LINE_LENGTH);
    }

    const size_t TAIL_SIZE = 4096 - LINE_LENGTH * nrecords;
    char file_tail[TAIL_SIZE];
    ret = fread(file_tail, 1, TAIL_SIZE, categories_fp);
    assert(ret == TAIL_SIZE);
    for (size_t i = 0; i < TAIL_SIZE; i++) {
        assert(file_tail[i] == 0);
    }
    ret = fread(file_tail, 1, 1, categories_fp);
    assert(ret == 0);
    fclose(categories_fp);
}


void check_xattr(const char* path, const char* key, const char* value) {
    ssize_t value_size = getxattr(path, key, NULL, 0);
    assert(value_size != -1);
    char* read_value = calloc(value_size + 1, 1);
    value_size = getxattr(path, key, read_value, value_size);
    assert(value_size != -1);
    assert(0 == strcmp(value, read_value));
}
