#!/bin/bash
set -x
fusermount -uz /mnt/vitis
umount /etc/vitis/loop81
rm -rf /etc/vitis
losetup -d /dev/loop81
rm /tmp/ds.fs
