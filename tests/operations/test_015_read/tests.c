#include <assert.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../../common.h"


void case01();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


// write()
void case01() {
    const char* FILE1 = MP"category1/file1";
    char* str1 = "But I being poor have only my dreams\n";
    char* str2 = "I have spread my dreams under your feet\n";
    char* str3 = "Thead softly, because you tread on my dreams\n";

    // check start content of file1
    int fd1 = open(FILE1, O_RDONLY);
    char buffer[1024];
    memset(buffer, '\0', 1024);
    int nbytes = read(fd1, buffer, 1024);  // file is not empty already
    assert(nbytes == strlen(str1));
    assert(0 == strcmp(str1, buffer));

    // write all lines
    int fd2 = open(FILE1, O_WRONLY | O_APPEND | O_TRUNC);
    assert(fd2 != -1);
    write(fd2, str1, strlen(str1));
    write(fd2, str2, strlen(str2));
    write(fd2, str3, strlen(str3));

    // read
    fd1 = open(FILE1, O_RDONLY);
    memset(buffer, '\0', 1024);
    nbytes = read(fd1, buffer, 1024);
    assert(nbytes == strlen(str1) + strlen(str2) + strlen(str3));
    assert(0 == strncmp("But I being poor", buffer, 16));
}
