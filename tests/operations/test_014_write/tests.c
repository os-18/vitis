#include <assert.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../../common.h"


void case01();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


// write()
void case01() {
    const char* FILE1 = MP"category1/file1";
    const char* FILE2 = MP"category1/file2";

    int fd1 = open(FILE1, O_WRONLY);
    assert(fd1 != -1);
    int fd2 = open(FILE2, O_WRONLY);
    assert(fd2 != -1);

    char* str1 = "But I being poor have only my dreams\n";
    int ret = write(fd1, str1, strlen(str1));
    assert(strlen(str1) == ret);

    struct stat file1_st;
    if (-1 == lstat(FILE1, &file1_st)) {
        perror("lstat");
        assert(0);
    }
    assert(file1_st.st_size == ret);

    int fd3 = open(FILE1, O_WRONLY | O_APPEND);
    char* str2 = "I have spread my dreams under your feet\n";
    char* str3 = "Thead softly, because you tread on my dreams\n";
    ret = write(fd3, str2, strlen(str2));
    assert(strlen(str2) == ret);
    ret = write(fd3, str3, strlen(str3));
    assert(strlen(str3) == ret);

    if (-1 == lstat(FILE1, &file1_st)) {
        perror("lstat");
        assert(0);
    }
    assert(file1_st.st_size == strlen(str1) + strlen(str2) + strlen(str3));
}
