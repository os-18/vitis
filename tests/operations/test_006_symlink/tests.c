#include <assert.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../../common.h"


void case01();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


// symlink(), creating symlink to regular file
void case01() {
    const char* FILE1             = MP"category1/file1";
    const char* CATEGORY1_NODES   = DS".vitis/00/00/00/00/02/nodes";
    const char* CATEGORY1_CATS    = DS".vitis/00/00/00/00/02/categories";
    const char* CATEGORY1_SUBCATS = DS".vitis/00/00/00/00/02/subcategories";
    const char* SYMLINK1          = MP"category1/symlink1";
    const char* SYMLINK1_IN_DS    = DS".vitis/00/00/00/00/04/";
    const char* SYMLINK1_FILE     = DS".vitis/00/00/00/00/04/file";
    const char* SYMLINK1_CATS     = DS".vitis/00/00/00/00/04/categories";
    const char* SYMLINK1_SUBCATS  = DS".vitis/00/00/00/00/04/subcategories";
    const char* SYMLINK1_NODES    = DS".vitis/00/00/00/00/04/nodes";

    if (-1 == symlink(FILE1, SYMLINK1)) {
        perror("creat");
        assert(0);
    }
    assert(F_OK == faccessat(AT_FDCWD, SYMLINK1, 0, AT_SYMLINK_NOFOLLOW));

    assert(0 == access(SYMLINK1_IN_DS, 0));
    assert(0 == access(SYMLINK1_FILE, 0));
    char link_pointer[256];
    memset(link_pointer, 0, 256);
    assert(-1 != readlink(SYMLINK1_FILE, link_pointer, 256));
    assert(0 == strcmp(link_pointer, FILE1));

    struct stat st;
    assert(0 == lstat(SYMLINK1_FILE, &st));
    assert((st.st_mode & S_IFMT) == S_IFLNK);

    // state of category files is not changed
    assertZero4KiB(CATEGORY1_SUBCATS);
    check_dds_file(CATEGORY1_CATS, 1, "category1", 1, 1);
    // nodes file changed
    check_dds_file(CATEGORY1_NODES, 3, "file1", 1, 2);
    check_dds_file(CATEGORY1_NODES, 4, "symlink1", 2, 2);

    // special directory of new file
    assert(-1 == access(SYMLINK1_SUBCATS, 0));  // fantastic file
    assert(-1 == access(SYMLINK1_NODES, 0));  // fantastic file
    check_dds_file(SYMLINK1_CATS, 2, "symlink1", 1, 1);
}
