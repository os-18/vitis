#include <assert.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../../common.h"


void case01();
void case02();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        case 2: case02(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


// open()
void case01() {
    const char* FILE1 = MP"category1/file1";
    const char* FILE2 = MP"category1/file2";

    int fd1 = open(FILE1, O_RDONLY);
    assert(fd1 != -1);
    int fd2 = open(FILE2, O_RDONLY);
    assert(fd2 != -1);
    assert(fd1 != fd2);
    int fd3 = open(FILE1, O_WRONLY);
    assert(fd3 != -1);
    int fd4 = open(FILE1, O_RDWR);
    assert(fd4 != -1);

    if (getuid() != 0) {
        int fd5 = open(FILE2, O_WRONLY); // no permissions
        assert(errno == EACCES);
        assert(fd5 == -1);
    }
}


// open(), file's note with "\\note"
void case02() {
    const char* REQUEST = MP"category1/file1\\note";
    // for reading
    int fd = open(REQUEST, O_RDONLY);
    if (fd == -1) {
        perror("open");
        assert(0);
    }
    close(fd);

    // now note is empty
    char buffer[512];
    read_text(REQUEST, buffer, 512);
    assert(0 == strcmp("", buffer));
    memset(buffer, '\0', 512);

    // writing
    fd = open(REQUEST, O_WRONLY);
    if (fd == -1) {
        perror("open");
        assert(0);
    }
    const char* note_content = "\"He Wishes for the Cloths of Heaven\" is\
a poem by William Butler Yeats. \
It was published in 1899 in his third volume of poetry, \
\"The Wind Among the Reeds\".";
    if (-1 == write(fd, note_content, strlen(note_content))) {
        perror("write");
        assert(0);
    }
    close(fd);
    read_text(REQUEST, buffer, 512);
    assert(0 == strcmp(note_content, buffer));
}

