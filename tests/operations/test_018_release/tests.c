#include <assert.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../../common.h"


void case01();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


void case01() {
    const char* FILE1 = MP"category1/file1";

    struct stat st;
    if (-1 == lstat(FILE1, &st)) {
        perror("lstat");
        assert(0);
    }
    assert(st.st_size > 0);

    int fd = open(FILE1, O_WRONLY);
    if (-1 == fd) {
        perror("open");
        assert(0);
    }
    int ret = close(fd);
    assert(ret == 0);
}
