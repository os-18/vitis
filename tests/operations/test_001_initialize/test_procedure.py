import os

import pytest

from conftest import fs_empty_environment, run_c_test


DIR = os.path.dirname(__file__)


def run(case_id: int):
    run_c_test(case_id, DIR)


def test_case01(fs_empty_environment):
    run(1)


def test_case02(fs_empty_environment):
    run(2)


def test_case03(fs_empty_environment):
    run(3)
