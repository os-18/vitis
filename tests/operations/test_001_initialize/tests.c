#include <assert.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../../common.h"


void case01();
void case02();
void case03();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        case 2: case02(); break;
        case 3: case03(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


/// .vitis, subdirectories, special files (existence) in primary mount point
void case01() {
    assert(0 == access(DS, 0));
    chdir(DS);

    assert(0 == access(".vitis/", 0));
    assert(0 == access(".vitis/00/00/00/00/00/", 0));
    assert(0 == access(".vitis/00/00/00/00/01/", 0));
    assert(0 == access(".vitis/00/00/00/00/01/attrs", 0));
    assert(0 == access(".vitis/00/00/00/00/01/categories", 0));
    assert(0 == access(".vitis/00/00/00/00/01/subcategories", 0));
    assert(0 == access(".vitis/00/00/00/00/01/nodes", 0));
}


// attrs
void case02() {
    const char* expected = NULL;
    if (-1 == chdir("/etc/vitis/loop81/")) {
        assert(0);
    }
    FILE* fp = fopen(".vitis/00/00/00/00/01/attrs", "r");
    assert(fp != NULL);

    // header
    expected = R"("key","value")""\n";
    check_next_line(fp, expected);

    // vitis.canon_name
    expected = R"("vitis.canon_name","/")""\n";
    check_next_line(fp, expected);

    // vitis.filetype
    expected = R"("vitis.filetype","d")""\n";
    check_next_line(fp, expected);

    // vitis.filemode
    expected = R"("vitis.filemode","000111111101")""\n";  // if umask is 0002
    check_next_line(fp, expected);

    // vitis.userid
    expected = R"("vitis.userid","0")""\n";  // root user is 0
    check_next_line(fp, expected);

    // vitis.groupid
    expected = R"("vitis.groupid","0")""\n";  // root group is 0
    check_next_line(fp, expected);

    // vitis.stat.nlink
    expected = R"("vitis.stat.nlink","2")""\n";
    check_next_line(fp, expected);

    char* line;
    size_t len;
    ssize_t rlen;

    rlen = getline(&line, &len, fp);
    assert(starts_with(line, "\"vitis.stat.btime\",\""));
    free(line);
    rlen = getline(&line, &len, fp);
    assert(starts_with(line, "\"vitis.stat.mtime\",\""));
    free(line);
    rlen = getline(&line, &len, fp);
    assert(starts_with(line, "\"vitis.stat.atime\",\""));
    free(line);
    rlen = getline(&line, &len, fp);
    assert(starts_with(line, "\"vitis.stat.ctime\",\""));
    free(line);

    rlen = getline(&line, &len, fp);
    assert(rlen == -1);  // end of file
    fclose(fp);
}


void case03() {
    assert(0 == access("/etc/vitis/loop81/", 0));
    chdir("/etc/vitis/loop81/");

    assertZero4KiB(".vitis/00/00/00/00/01/categories");
    assertZero4KiB(".vitis/00/00/00/00/01/subcategories");
    assertZero4KiB(".vitis/00/00/00/00/01/nodes");
}
