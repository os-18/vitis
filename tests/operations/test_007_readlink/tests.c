#include <assert.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../../common.h"


void case01();
void case02();
void case03();
void case04();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        case 2: case02(); break;
        case 3: case03(); break;
        case 4: case04(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


// readlink(), one symbolic link in one category
void case01() {
    const char* SYMLINK1 = MP"category1/symlink1";
    const char* expected = "/usr/bin/gcc-next";
    char buffer[256];
    memset(buffer, '\0', 256);
    if (-1 == readlink(SYMLINK1, buffer, 256)) {
        perror("readlink");
        assert(0);
    }
    assert(0 == strcmp(buffer, expected));
}


// readlink(), one symbolic link to existent non-Vitis directory
void case02() {
    const char* SYMLINK1 = MP"category1/symlink1";
    const char* expected = "/usr/bin/";
    char buffer[256];
    memset(buffer, '\0', 256);
    if (-1 == readlink(SYMLINK1, buffer, 256)) {
        perror("readlink");
        assert(0);
    }
    assert(0 == strcmp(buffer, expected));
}


// readlink(), symbolic link to non-existent category
void case03() {
    const char* BROKEN_SYMLINK2 = MP"symlink2";
    const char* expected = "non-existent/";
    char buffer[256] = {0};
    if (-1 == readlink(BROKEN_SYMLINK2, buffer, 256)) {
        perror("readlink");
        assert(0);
    }
    assert(0 == strcmp(buffer, expected));
}


// readlink(), imaginary link
void case04() {
    const char* IM_LINK = MP"category1\\l";
    const char* expected = ":i2:";
    char buffer[256] = {0};
    if (-1 == readlink(IM_LINK, buffer, 256)) {
        perror("readlink");
        assert(0);
    }
    strcmp_assert(buffer, expected);

    // strange case
    IM_LINK = MP"category1\\l\\l\\l\\l";
    memset(buffer, '\0', 256);
    if (-1 == readlink(IM_LINK, buffer, 256)) {
        perror("readlink");
        assert(0);
    }
    strcmp_assert(buffer, expected);
}
