#include <assert.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../../common.h"


void case01();
void case02();
void case03();
void case04();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        case 2: case02(); break;
        case 3: case03(); break;
        case 4: case04(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


// create(), creating regular file
void case01() {
    const char* FILE1             = MP"category1/file1";
    const char* CATEGORY1_IN_DS   = DS".vitis/00/00/00/00/02/";
    const char* CATEGORY1_NODES   = DS".vitis/00/00/00/00/02/nodes";
    const char* CATEGORY1_CATS    = DS".vitis/00/00/00/00/02/categories";
    const char* CATEGORY1_SUBCATS = DS".vitis/00/00/00/00/02/subcategories";
    const char* FILE1_IN_DS       = DS".vitis/00/00/00/00/03/";
    const char* FILE1_FILE        = DS".vitis/00/00/00/00/03/file";
    const char* FILE1_CATS        = DS".vitis/00/00/00/00/03/categories";
    const char* FILE1_SUBCATS     = DS".vitis/00/00/00/00/03/subcategories";
    const char* FILE1_NODES       = DS".vitis/00/00/00/00/03/nodes";

    assertZero4KiB(CATEGORY1_SUBCATS);
    check_dds_file(CATEGORY1_CATS, 1, "category1", 1, 1);
    assertZero4KiB(CATEGORY1_NODES);

    if (-1 == creat(FILE1, 0777)) {
        perror("creat");
        assert(0);
    }

    assert(0 == access(FILE1_IN_DS, 0));
    assert(0 == access(FILE1_FILE, 0));

    struct stat st;
    assert(0 == lstat(FILE1_FILE, &st));
    assert((st.st_mode & S_IFMT) == S_IFREG);

    // state of category files is not changed
    assertZero4KiB(CATEGORY1_SUBCATS);
    check_dds_file(CATEGORY1_CATS, 1, "category1", 1, 1);
    // nodes file changed
    check_dds_file(CATEGORY1_NODES, 3, "file1", 1, 1);

    // special directory of new file
    assert(-1 == access(FILE1_SUBCATS, 0));  // fantastic file
    assert(-1 == access(FILE1_NODES, 0));  // fantastic file
    check_dds_file(FILE1_CATS, 2, "file1", 1, 1);
}


// create(), wrong using: node in autocategory
void case02() {
    const char* NONEXISTENT = MP"@/Extension/txt/non-existent.txt";
    errno = 0;
    int ret = creat(NONEXISTENT, 0777);
    int err = errno;
    assert(-1 == ret);
    assert(err == EACCES);
}


// create(), wrong using: path contains unacceptable symbol (LF)
void case03() {
    const char* FILE1 = MP"category1/file1\nsubtitle";
    int ret = creat(FILE1, 0777);
    assert(-1 == ret && errno == EINVAL);
    assert(-1 == access(DS".vitis/00/00/00/00/03/", 0));
}


void case04() {
    assert(-1 == access(DS".vitis/00/00/00/00/03/note", 0));
    const char* FILE1 = MP"category1/file1\\note";
    int fd = creat(FILE1, 0777);
    assert(-1 != fd);
    assert(0 == access(DS".vitis/00/00/00/00/03/note", 0));
}
