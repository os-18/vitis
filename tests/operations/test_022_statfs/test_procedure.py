import inspect
import os
import sys
sys.path.append('../')

import pytest

from conftest import fs_empty_environment, MP, run_c_test
from common import get_caseno

CATEGORY1 = os.path.join(MP, 'category1')


def run(case_id: int):
    run_c_test(case_id, os.path.dirname(__file__))


def test_case01(fs_empty_environment):
    """
    statvfs()
    """
    os.mkdir(CATEGORY1)
    run(get_caseno())
