import inspect
import os
import sys
sys.path.append('../')

import pytest

from conftest import fs_empty_environment, MP, run_c_test
import common, conftest

CATEGORY1 = os.path.join(MP, 'category1')
FILE1 = os.path.join(CATEGORY1, 'file1')
FILE2 = os.path.join(CATEGORY1, 'file2')
SYMLINK1 = os.path.join(CATEGORY1, 'symlink1')
butI = 'But I being poor have only my dreams\n'

DIR = os.path.dirname(__file__)


def run(case_id: int):
    run_c_test(case_id, DIR)


def test_case01(fs_empty_environment):
    """
    mkdir(), new category
    """
    caseno = int(inspect.stack()[0][3].split('test_case')[-1])
    run(caseno)


def test_case02(fs_empty_environment):
    """
    mkdir(), wrong using: category with strange symbols
    """
    caseno = int(inspect.stack()[0][3].split('test_case')[-1])
    run(caseno)


def test_case03(fs_empty_environment):
    """
    mkdir(), wrong using: new autocategory
    """
    caseno = int(inspect.stack()[0][3].split('test_case')[-1])
    run(caseno)


def test_case04(fs_empty_environment):
    """
    mkdir(), new category and new subcategory
    """
    caseno = int(inspect.stack()[0][3].split('test_case')[-1])
    run(caseno)

