#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../../common.h"


void case01();
void case02();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        case 2: case02(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


// opendir()
void case01() {
    const char* CATEGORY1 = MP"category1";
    const char* CATEGORY2 = MP"category2";

    errno = 0;
    DIR* dirstream = opendir(CATEGORY1);
    assert(dirstream != NULL && errno == 0);

    DIR* dirstream2 = opendir(CATEGORY2);
    assert(dirstream2 == NULL && errno == ENOENT);
}


// opendir(), name list
void case02() {
    const char* NAME_LIST = MP"Poetry/yeats.txt\\names";
    errno = 0;
    DIR* dirstream = opendir(NAME_LIST);
    assert(dirstream != NULL && errno == 0);
}
