#include <assert.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../../common.h"


void case01();
void case02();
void case03();
void case04();
void case05();
void case06();
void case07();
void case08();
void case09();
void case10();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        case 2: case02(); break;
        case 3: case03(); break;
        case 4: case04(); break;
        case 5: case05(); break;
        case 6: case06(); break;
        case 7: case07(); break;
        case 8: case08(); break;
        case 9: case09(); break;
        case 10: case10(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


// lstat(), one hard link in one category
void case01() {
    const char* FILE1 = MP"category1/file1";
    const char* FILE1_IN_DS = DS".vitis/00/00/00/00/03/file";
    struct stat st, orig_st;
    if (-1 == lstat(FILE1, &st)) {
        perror("lstat");
        assert(0);
    }
    assert((st.st_mode & S_IFMT) == S_IFREG);
    if (-1 == lstat(FILE1_IN_DS, &orig_st)) {
        perror("lstat");
        assert(0);
    }
    assert(st.st_nlink == 2);  // node by default has two links as minimum
    assert(st.st_size == 37 && st.st_size == orig_st.st_size);
    assert(st.st_dev != orig_st.st_dev);
}



// lstat(), two hard link in one category
void case02() {
    const char* FILE1 = MP"category1/file1";
    const char* FILE2 = MP"category1/file2";
    struct stat st1, st2;
    if (-1 == lstat(FILE1, &st1)) {
        perror("lstat, file1");
        assert(0);
    }
    assert((st1.st_mode & S_IFMT) == S_IFREG);
    if (-1 == lstat(FILE2, &st2)) {
        perror("lstat, file2");
        assert(0);
    }
    assert((st2.st_mode & S_IFMT) == S_IFREG);

    assert(st1.st_nlink == 2+1 && st1.st_nlink == st2.st_nlink);
    assert(st1.st_size == 37 && st1.st_size == st2.st_size);
    assert(st1.st_dev == st2.st_dev);
    assert(st1.st_ino == st2.st_ino);
}


// lstat(), one category
void case03() {
    const char* CATEGORY1 = MP"category1/";
    const char* CATEGORY1_IN_DS = DS".vitis/00/00/00/00/02/";
    struct stat st, orig_st;
    if (-1 == lstat(CATEGORY1, &st)) {
        perror("lstat, category1");
        assert(0);
    }
    assert((st.st_mode & S_IFMT) == S_IFDIR);
    lstat(CATEGORY1_IN_DS, &orig_st);

    assert(st.st_nlink == 2);
    assert(st.st_size == 0);
    assert(st.st_dev != orig_st.st_dev);
}


// lstat(), one symbolic link to regular file
void case04() {
    const char* FILE1 = MP"category1/file1";
    const char* SYMLINK1 = MP"category1/symlink1";
    struct stat sl_st, regfile_st;
    if (-1 == lstat(SYMLINK1, &sl_st)) {
        perror("lstat");
        assert(0);
    }
    assert((sl_st.st_mode & S_IFMT) == S_IFLNK);
    lstat(FILE1, &regfile_st);
    assert(sl_st.st_nlink == 1+1 && sl_st.st_nlink != regfile_st.st_nlink);
    assert(sl_st.st_ino != regfile_st.st_ino);
}


// stat(), one symbolic link to regular file
void case05() {
    const char* FILE1 = MP"category1/file1";
    const char* SYMLINK1 = MP"category1/symlink1";
    struct stat sl_st, regfile_st;
    if (-1 == stat(SYMLINK1, &sl_st)) {
        perror("stat");
        assert(0);
    }
    assert((sl_st.st_mode & S_IFMT) == S_IFREG);
    lstat(FILE1, &regfile_st);
    assert(sl_st.st_nlink == 2 && sl_st.st_nlink == regfile_st.st_nlink);
    assert(sl_st.st_ino == regfile_st.st_ino);
}


// lstat(), imaginary link to directory
void case06() {
    const char* IM_LINK = MP"category1\\l";
    struct stat st;
    if (-1 == lstat(IM_LINK, &st)) {
        perror("lstat");
        assert(0);
    }
    assert((st.st_mode & S_IFMT) == S_IFLNK);

    // strange case
    IM_LINK = MP"category1\\l\\l\\l\\l";
    if (-1 == lstat(IM_LINK, &st)) {
        perror("lstat");
        assert(0);
    }
    assert((st.st_mode & S_IFMT) == S_IFLNK);
}


// stat(), imaginary link to directory
void case07() {
    const char* IM_LINK = MP"category1\\l";
    struct stat st;
    if (-1 == stat(IM_LINK, &st)) {
        perror("stat");
        assert(0);
    }
    assert((st.st_mode & S_IFMT) == S_IFDIR);

    // strange case
    IM_LINK = MP"category1\\l\\l\\l\\l";
    if (-1 == stat(IM_LINK, &st)) {
        perror("stat");
        assert(0);
    }
    assert((st.st_mode & S_IFMT) == S_IFDIR);
}


// lstat(), repeated name: error case
void case08() {
    const char* REQUEST = MP"category1\\r";
    struct stat st;
    int ret = lstat(REQUEST, &st);
    assert(ret == -1 && errno == ENOENT);
}


// lstat(), file name list
void case09() {
    const char* ORIG_FILE = MP"Poetry/yeats.txt";
    const char* REQUEST_LIST = MP"Poetry/yeats.txt\\names";
    const char* REQUEST_FILE = MP"Poetry/yeats.txt\\names/:c2:yeats.txt";
    struct stat st;
    int ret;
    errno = 0;
    ret = lstat(REQUEST_LIST, &st);
    assert(ret == 0 && errno == 0);

    struct stat file_st;
    ret = lstat(REQUEST_FILE, &file_st);
    assert(ret == 0 && errno == 0);
    struct stat orig_st;
    ret = lstat(ORIG_FILE, &orig_st);
    assert(ret == 0 && errno == 0);
    assert(file_st.st_ino == orig_st.st_ino);
}


// lstat(), inode id path
void case10() {
    const char* ORIG_FILE = MP"Poetry/yeats.txt";
    const char* REQUEST1 = MP":i2:/:i8:yeats.txt";
    const char* REQUEST2 = MP":i2:Poetry/:i8:yeats.txt";
    const char* REQUEST3 = MP":i2:/:i8:";
    const char* REQUEST4 = MP":i2:Poetry/:i8:";
    struct stat orig_st, req_st;
    int ret;
    ret = lstat(ORIG_FILE, &orig_st);
    assert(ret == 0 && errno == 0);

    ret = lstat(REQUEST1, &req_st);
    assert(ret == 0 && errno == 0);
    assert(1 == equal_stats(&orig_st, &req_st, 1));

    ret = lstat(REQUEST2, &req_st);
    assert(ret == 0 && errno == 0);
    assert(1 == equal_stats(&orig_st, &req_st, 1));

    ret = lstat(REQUEST3, &req_st);
    assert(ret == 0 && errno == 0);
    assert(1 == equal_stats(&orig_st, &req_st, 1));

    ret = lstat(REQUEST4, &req_st);
    assert(ret == 0 && errno == 0);
    assert(1 == equal_stats(&orig_st, &req_st, 1));
}
