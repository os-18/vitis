import os
import sys
sys.path.append('../')

import pytest

from conftest import fs_empty_environment, MP, run_c_test
import common
from common import get_caseno

CATEGORY1 = os.path.join(MP, 'category1')
FILE1 = os.path.join(CATEGORY1, 'file1')
FILE2 = os.path.join(CATEGORY1, 'file2')
SYMLINK1 = os.path.join(CATEGORY1, 'symlink1')
butI = 'But I being poor have only my dreams\n'


def run(case_id: int):
    run_c_test(case_id, os.path.dirname(__file__))


def test_case01(fs_empty_environment):
    """
    lstat(), one hard link in one category
    """
    os.mkdir(CATEGORY1)
    common.write_to_file(FILE1, butI)
    run(get_caseno())


def test_case02(fs_empty_environment):
    """
    lstat(), two hard link in one category
    """
    os.mkdir(CATEGORY1)
    common.write_to_file(FILE1, butI)
    os.link(FILE1, FILE2)
    run(get_caseno())


def test_case03(fs_empty_environment):
    """
    lstat(), one category
    """
    os.mkdir(CATEGORY1)
    run(get_caseno())


def test_case04(fs_empty_environment):
    """
    lstat(), one symbolic link to regular file
    """
    os.mkdir(CATEGORY1)
    common.write_to_file(FILE1, butI)
    os.link(FILE1, FILE2)
    os.symlink('file1', SYMLINK1)
    run(get_caseno())


def test_case05(fs_empty_environment):
    """
    stat(), one symbolic link to regular file
    """
    os.mkdir(CATEGORY1)
    common.write_to_file(FILE1, butI)
    os.symlink(FILE1, SYMLINK1)
    run(get_caseno())


def test_case06(fs_empty_environment):
    """
    lstat(), imaginary link to directory
    """
    os.mkdir(CATEGORY1)
    run(get_caseno())


def test_case07(fs_empty_environment):
    """
    stat(), imaginary link to directory
    """
    os.mkdir(CATEGORY1)
    run(get_caseno())


def test_case08(fs_empty_environment):
    """
    lstat(), repeated name: error case
    """
    os.mkdir(CATEGORY1)
    run(get_caseno())
