#include <assert.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../../common.h"


void case01();
void case02();
void case03();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        case 2: case02(); break;
        case 3: case03(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


// unlink(), deleting file
void case01() {
    const char* FILE1 = MP"category1/file1";
    assert(0 == access(FILE1, 0));

    if (-1 == unlink(FILE1)) {
        perror("unlink");
        assert(0);
    }

    assert(0 != access(FILE1, 0));
}


// unlink(), deleting directory
void case02() {
    const char* CATEGORY1 = MP"category1";
    const char* CATEGORY1_IMLINK = MP"category1\\l";
    const char* CATEGORY1_2ND = MP"category1_2nd";
    assert(0 == link(CATEGORY1_IMLINK, CATEGORY1_2ND));
    struct stat st;
    assert(0 == stat(CATEGORY1, &st));
    assert(st.st_nlink == 3);  // default primary value

    int ret = unlink(CATEGORY1);
    assert(ret == -1 && errno == EISDIR);
}


void case03() {
}
