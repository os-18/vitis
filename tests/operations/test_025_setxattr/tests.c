#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/statvfs.h>
#include <sys/xattr.h>
#include <unistd.h>

#include "../../common.h"


void case01();
void case02();
void case03();
void case04();
void case05();
void case06();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        case 2: case02(); break;
        case 3: case03(); break;
        case 4: case04(); break;
        case 5: case05(); break;
        case 6: case06(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


// setxattr(), simple case
void case01() {
    const char* CATEGORY1 = MP"category1";
    const char* KEY = "user.mark.future";
    const char* VALUE = "true";
    int ret = setxattr(CATEGORY1, KEY, VALUE, strlen(VALUE), 0);
    assert(ret == 0);

    // check with getxattr
    check_xattr(CATEGORY1, KEY, VALUE);
}


// setxattr(), forbidden case
void case02() {
    const char* CATEGORY1 = MP"category1";
    ssize_t value_size = getxattr(CATEGORY1, "user.strange_key", NULL, 0);
    assert(value_size == -1 && errno == ENODATA);
}


// setxattr(), XATTR_CREATE
void case03() {  //like case01, but with XATTR_CREATE
    const char* CATEGORY1 = MP"category1";
    const char* KEY = "user.mark.future";
    const char* VALUE = "true";
    int ret = setxattr(CATEGORY1, KEY, VALUE, strlen(VALUE), XATTR_CREATE);
    assert(ret == 0);

    // check with getxattr
    check_xattr(CATEGORY1, KEY, VALUE);
}


// setxattr(), error case with XATTR_CREATE
void case04() {
    const char* CATEGORY1 = MP"category1";
    const char* KEY = "user.mark.future";
    const char* VALUE = "true";
    const char* NEW_VALUE = "false";
    int ret;
    ret = setxattr(CATEGORY1, KEY, VALUE, strlen(VALUE), XATTR_CREATE);
    assert(ret == 0);
    ret = setxattr(CATEGORY1, KEY, NEW_VALUE, strlen(NEW_VALUE), XATTR_CREATE);
    assert(ret != 0 && errno == EEXIST);

    // check with getxattr, value is "true"
    check_xattr(CATEGORY1, KEY, VALUE);
}


// setxattr(), XATTR_REPLACE
void case05() {
    const char* CATEGORY1 = MP"category1";
    const char* KEY = "user.mark.future";
    const char* VALUE = "true";
    const char* NEW_VALUE = "false";
    int ret;
    ret = setxattr(CATEGORY1, KEY, VALUE, strlen(VALUE), XATTR_CREATE);
    assert(ret == 0);
    ret = setxattr(CATEGORY1, KEY, NEW_VALUE, strlen(NEW_VALUE), XATTR_REPLACE);
    assert(ret == 0);

    // check with getxattr, value is "false"
    check_xattr(CATEGORY1, KEY, NEW_VALUE);
}


// setxattr(), error case with XATTR_REPLACE
void case06() {
    const char* CATEGORY1 = MP"category1";
    const char* KEY = "user.mark.future";
    const char* NEW_VALUE = "false";
    int ret;
    ret = setxattr(CATEGORY1, KEY, NEW_VALUE, strlen(NEW_VALUE), XATTR_REPLACE);
    assert(ret == -1 && errno == ENODATA);

    // check with getxattr
    ssize_t value_size = getxattr(CATEGORY1, KEY, NULL, 0);
    assert(value_size == -1 && errno == ENODATA);
}
