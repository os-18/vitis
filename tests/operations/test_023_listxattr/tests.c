#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/statvfs.h>
#include <sys/xattr.h>
#include <unistd.h>

#include "../../common.h"


void case01();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


// listxattr
void case01() {
    const char* CATEGORY1 = MP"category1";

    char* buffer;
    ssize_t buffer_size;

    buffer_size = listxattr(CATEGORY1, NULL, 0);
    fprintf(stderr, "buffer_size: %lld\n", buffer_size);
    if (buffer_size == -1) {
        perror("listxattr");
        assert(0);
    }
    if (buffer_size == 0) {
        fprintf(stderr, "No attributes.\n");
        assert(0);
    }
    buffer = malloc(buffer_size);
    if (buffer == NULL) {
        perror("malloc");
        assert(0);
    }
    buffer_size = listxattr(CATEGORY1, buffer, buffer_size);
    if (buffer_size == -1) {
        perror("listxattr (2)");
        assert(0);
    }

    size_t key_number = 10;
    char* keys[10] = {
        "vitis.canon_name", "vitis.filetype", "vitis.filemode",
        "vitis.userid", "vitis.groupid", "vitis.stat.nlink",
        "vitis.stat.btime", "vitis.stat.mtime",
        "vitis.stat.atime", "vitis.stat.ctime"
    };

    char* key = buffer;
    size_t index = 0;
    while (buffer_size > 0 && index < 10) {
        assert(0 == strcmp(key, keys[index]));
        ssize_t key_size = strlen(key) + 1;
        buffer_size -= key_size;
        key += key_size;
        index++;
    }
    assert(buffer_size == 0);
    free(buffer);
}
