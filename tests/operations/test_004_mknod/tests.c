#include <assert.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../../common.h"


void case01();
void case02();
void case03();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        case 2: case02(); break;
        case 3: case03(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


// mknod(), creating named pipe
void case01() {
    const char* PIPE1             = MP"category1/pipe1";
    const char* CATEGORY1_IN_DS   = DS".vitis/00/00/00/00/02/";
    const char* CATEGORY1_NODES   = DS".vitis/00/00/00/00/02/nodes";
    const char* CATEGORY1_CATS    = DS".vitis/00/00/00/00/02/categories";
    const char* CATEGORY1_SUBCATS = DS".vitis/00/00/00/00/02/subcategories";
    const char* PIPE1_IN_DS       = DS".vitis/00/00/00/00/03/";
    const char* PIPE1_FILE        = DS".vitis/00/00/00/00/03/file";
    const char* PIPE1_CATS        = DS".vitis/00/00/00/00/03/categories";
    const char* PIPE1_SUBCATS     = DS".vitis/00/00/00/00/03/subcategories";
    const char* PIPE1_NODES       = DS".vitis/00/00/00/00/03/nodes";

    assertZero4KiB(CATEGORY1_SUBCATS);
    check_dds_file(CATEGORY1_CATS, 1, "category1", 1, 1);
    assertZero4KiB(CATEGORY1_NODES);

    if (-1 == mknod(PIPE1, S_IFIFO | 0777, 0)) {
        perror("mknod");
        assert(0);
    }

    assert(0 == access(PIPE1_IN_DS, 0));
    assert(0 == access(PIPE1_FILE, 0));

    struct stat st;
    assert(0 == lstat(PIPE1_FILE, &st));
    assert((st.st_mode & S_IFMT) == S_IFIFO);

    // state of category files is not changed
    assertZero4KiB(CATEGORY1_SUBCATS);
    check_dds_file(CATEGORY1_CATS, 1, "category1", 1, 1);
    // nodes file changed
    check_dds_file(CATEGORY1_NODES, 3, "pipe1", 1, 1);

    // special directory of new pipe
    assert(-1 == access(PIPE1_SUBCATS, 0));  // fantastic file
    assert(-1 == access(PIPE1_NODES, 0));  // fantastic file
    check_dds_file(PIPE1_CATS, 2, "pipe1", 1, 1);
}


// mknod(), wrong using: node in autocategory
void case02() {
    const char* NONEXISTENT = MP"@/Extension/txt/non-existent.txt";
    errno = 0;
    int ret = mknod(NONEXISTENT, S_IFIFO | 0777, 0);
    int err = errno;
    assert(-1 == ret);
    assert(err == EACCES);
}


// mknod(), wrong using: path contains unacceptable symbol (LF)
void case03() {
    const char* PIPE1 = MP"category1/pipe1\nsubtitle";
    int ret = mknod(PIPE1, S_IFIFO | 0777, 0);
    if (-1 == ret) {
        assert(errno == EINVAL);
    }
    assert(-1 == access(DS".vitis/00/00/00/00/03/", 0));
}
