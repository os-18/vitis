#include <assert.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../../common.h"


void case01();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


// write()
void case01() {
    const char* FILE1 = MP"category1/file1";
    int fd = open(FILE1, O_RDONLY);
    if (-1 == fd) {
        perror("open");
        assert(0);
    }

    // SEEK_SET

    off_t offset1 = lseek(fd, 37, SEEK_SET);
    assert(offset1 == 37);

    char buffer[1024];
    memset(buffer, '\0', 1024);

    int readbytes = read(fd, buffer, 39);
    if (-1 == readbytes) {
        perror("read");
        assert(0);
    }
    assert(0 == strcmp(buffer, "I have spread my dreams under your feet"));


    // SEEK_CUR

    off_t offset2 = lseek(fd, 1, SEEK_CUR);  // after '\n'
    assert(offset2 == offset1 + readbytes + 1);

    memset(buffer, '\0', 1024);
    readbytes = read(fd, buffer, 12);
    if (-1 == readbytes) {
        perror("read");
        assert(0);
    }
    assert(0 == strcmp("Thead softly", buffer));


    // SEEK_END

    off_t offset3 = lseek(fd, -31, SEEK_END);
    memset(buffer, '\0', 1024);
    readbytes = read(fd, buffer, 30);
    if (-1 == readbytes) {
        perror("read");
        assert(0);
    }
    assert(0 == strcmp("because you tread on my dreams", buffer));
}
