#!/usr/bin/env bash

# This file is part of the Vitis project.
#
# Distributed under the GNU All-Permissive License.
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.
# This file is offered as-is, without any warranty.

DC=$1
REQUEST=$2
ARG=$3

AMALTHEA_MAJOR=1
OXFUSE_MAJOR=0

LDC2_STATIC_OPTS="-defaultlib=:libphobos2-ldc.a,:libdruntime-ldc.a,:libz.a"
LDC2_STATIC_OPTS+=" -link-defaultlib-shared=false"
GDC_STATIC_OPTS="-static-libphobos"
DMD_STATIC_OPTS=""

# ldc2 flags
LDC2_DEBUG_OPTIONS="-d-debug --gc"
LDC2_OPTIM_OPTIONS="-O --release --gc"
LDC2_OTHER_OPTIONS="-Jsource -J."
# dmd flags
DMD_DEBUG_OPTIONS="-debug -g"
DMD_OPTIM_OPTIONS="-O -release"
DMD_OTHER_OPTIONS="-Jsource -J."
# gdc flags
GDC_DEBUG_OPTIONS="-fdebug"
GDC_OPTIM_OPTIONS="-O2 -frelease"
GDC_OTHER_OPTIONS="-Jsource -J."


if [[ $REQUEST == "get_phobos_options" && $ARG == static ]]; then
    echo $(eval echo \$${DC^^}_STATIC_OPTS)
elif [[ $REQUEST == "get_amalthea_options" ]]; then
    [[ ${ARG} == dynamic ]] && LIBEXT=so.${AMALTHEA_MAJOR} || LIBEXT=a
    if [[ $DC != gdc ]]; then
        echo "-L-l:libamalthea-${DC}.${LIBEXT}"
    else
        echo "-lamalthea-${DC}"
    fi
elif [[ $REQUEST == "get_fuse_options" ]]; then
    [[ ${ARG} == dynamic ]] && LIBEXT=so.${OXFUSE_MAJOR} || LIBEXT=a
    if [[ $DC != gdc ]]; then
        echo "-L-l:liboxfuse-${DC}.${LIBEXT}"
    else
        echo "-loxfuse-${DC} -lfuse3"
    fi
elif [[ $REQUEST == "get_release_options" ]]; then
    echo $(eval echo \$${DC^^}_OPTIM_OPTIONS)
elif [[ $REQUEST == "get_debug_options" ]]; then
    echo $(eval echo \$${DC^^}_DEBUG_OPTIONS)
elif [[ $REQUEST == "get_other_options" ]]; then
    echo $(eval echo \$${DC^^}_OTHER_OPTIONS)
fi

if [[ $REQUEST == "get_doc_dir" ]]; then
    read -ra distr_id_arr <<< "$(lsb_release -i)"
    DISTRIBUTION=${distr_id_arr[2]}
    if [[ "${DISTRIBUTION}" != "openSUSE" ]]; then
        echo share/doc
    else
        echo share/doc/packages
    fi
fi
