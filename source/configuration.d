/* This file is part of the Vitis project.
 *
 * Copyright (C) Eugene 'Vindex' Stulin, 2018-2024
 *
 * Distributed under the Boost Software License 1.0 or (at your option)
 * the GNU General Public License 3.0 or later.
 * This file is offered as-is, without any warranty.
 */

module configuration;

static import std.algorithm;
static import std.file;

import
    std.array,
    std.path,
    std.process,
    std.string;

import amalthea.csv, amalthea.langlocal;

static import amalthea.dataprocessing;
static import amalthea.fs;
import amalthea.dataprocessing : isAmong;

static import filesystem;
import clibase;

static immutable defaultConfig = `
[Main settings]
language=auto
opener=ufo
casesensitivity=no
autocategorization=Format;Group;Extension
current_fs_file=
`;


/*******************************************************************************
 * The class for configuration management.
 */
final class VitisConf {
    static string vitisConfFile;

    static this() {
        auto confDir = buildPath(environment["HOME"], ".config/vitis");
        mkdirRecurse(confDir);
        vitisConfFile = buildPath(confDir, "vitis.conf");
    }

    static void setVitisConfFile(string filepath) {
        vitisConfFile = amalthea.fs.getAbsolutePath(filepath);
    }

    static string getConfPath() {
        string path = vitisConfFile;
        if (!amalthea.fs.exists(path)) {
            createConfFile(path);
        }
        return path;
    }

    deprecated alias fileConfPath = getConfPath;

    static void createConfFile(string path) {
        std.file.write(path, defaultConfig);
    }

    static void setDefault() {
        createConfFile(vitisConfFile);
    }

    private static string ConfGetter(string parameter) {
        parameter ~= "=";
        string confText = readText(getConfPath);
        if (!confText.endsWith("\n")) {
            confText ~= "\n";
        }
        auto paramPos = confText.indexOf(parameter);
        enforce!ValidException(
            paramPos != -1, "Parameter not found: " ~ parameter[0 .. $-1]
        );
        auto index1 = paramPos + parameter.length;
        auto index2 = index1 + confText[index1 .. $].indexOf("\n");
        return confText[index1 .. index2].dup;
    }

    static void ConfSetter(string parameter, string value) {
        string confText = readText(getConfPath);
        confText = confText.replace(parameter ~ "=" ~ ConfGetter(parameter),
                                    parameter ~ "=" ~ value);
        std.file.write(getConfPath, confText);
    }

    static string getLanguage()    { return ConfGetter("language");           }
    static string getOpener()      { return ConfGetter("opener");             }
    static string getCS()          { return ConfGetter("casesensitivity");    }
    static string getACMode()      { return ConfGetter("autocategorization"); }
    static string getCurrentFile() { return ConfGetter("current_fs_file");    }

    static void setPath    (string path)  { ConfSetter("path",     path); }
    static void setLanguage(string lang)  { ConfSetter("language", lang); }
    static void setOpener  (string opener){ ConfSetter("opener", opener); }
    static void setCurrentFile(string file) {
        ConfSetter("current_fs_file", file);
    }

    static string getMountPoint() {
        string image = getCurrentFile();
        assert(!image.empty);
        image = amalthea.fs.getAbsolutePath(image);
        // todo
        return "";
    }

    static void setAutosave(string autosave) {
        enforce(
            autosave.isAmong("yes", "no"),
            new ValidException("Invalid value for configuration parameter."._s)
        );
        ConfSetter("autosave", autosave);
    }

    static void setCaseSensitivity(string casesensitivity) {
        enforce(
            casesensitivity.isAmong("yes", "no"),
            new ConfException("Invalid value for configuration parameter."._s)
        );
        ConfSetter("casesensitivity", casesensitivity);
    }
}

