/* This file is part of the Vitis project.
 *
 * Copyright (C) Eugene 'Vindex' Stulin, 2021-2024
 *
 * Distributed under the Boost Software License 1.0 or (at your option)
 * the GNU General Public License 3.0 or later.
 * This file is offered as-is, without any warranty.
 */

pragma(lib, "fuse3");

import std.stdio : writeln;
import std.csv : csvReader;
import std.range : empty;
import std.string : strip;
import std.typecons : Tuple;

import amalthea.dataprocessing : extractOptionValue, isAmong;
import amalthea.langlocal : _s, initLocalization, getSystemLanguage;

import configuration : VitisConf;
import help : showHelp;

import
    cmd.show,
    cmd.assign,
    cmd.mkfs,
    cmd.mount,
    cmd.switcher;

import clibase;


alias Record = Tuple!(string, string, string);

static immutable string[][] translations = () {  // in compile time
    string[][] table;
    string csvText = import("res/translations.csv");
    foreach (record; csvReader!Record(csvText)) {
        table ~= [record[0], record[1], record[2]];
    }
    return table;
}();


/// Prints the software version.
void runVersionCommand() {
    writeln(import("version").strip);
}


/// Defines application language and sets configuration.
void setConfiguration(string configFile = "") {
    if (!configFile.empty) {
        VitisConf.setVitisConfFile(configFile);
    }
    string language = VitisConf.getLanguage();
    if (language == "auto") {
        language = getSystemLanguage();
    }
    initLocalization(translations, language);
}


/// Main function thats starts CLI commands by arguments.
void parseAndRunCommand(string[] args) {
    string command = args[1];
    if (args.length == 2) {
        if (command == "--version" || command == "version") {
            runVersionCommand();
            return;
        } else if (command.isAmong("--help", "-h", "help")) {
            showHelp();
            return;
        }
    }
    switch(command) {
        case "mkfs":   cmd.mkfs.runMkfsCommand(args);       break;
        case "mount":  cmd.mount.runMountCommand(args);     break;
        case "umount": cmd.mount.runUmountCommand(args);    break;
        case "switch": cmd.switcher.runSwitchCommand(args); break;
        case "show":   cmd.show.runShowCommand(args);       break;
        case "assign": cmd.assign.runAssignCommand(args);   break;
        default:
            auto messages = ["Invalid command."._s, "See: vitis --help"._s];
            throw new WrongUsage(messages);
    }
}


int main(string[] args) {
    if (args.length == 1) {
        tprintln!(TT.error)("Incorrect use of vitis."._s);
        tprintln!(TT.error)("See: vitis --help"._s);
        return WRONG_USAGE;
    }
    try {
        string configFile = extractOptionValue(args, "--conf");
        setConfiguration(configFile);
        parseAndRunCommand(args);
    } catch (WrongUsage e) {
        tprintln!(TT.error)(e.msg);
        return WRONG_USAGE;
    } catch (CommonError e) {
        tprintln!(TT.error)(e.msg);
        return COMMON_ERROR;
    } catch (Exception e) {
        debug {
            tprintln!(TT.error)(e);
        } else {
            tprintln!(TT.error)(e.msg);
        }
        return COMMON_ERROR;
    }
    return SUCCESS;
}
