/* This file is part of the Vitis project.
 *
 * Copyright (C) Eugene 'Vindex' Stulin, 2025
 *
 * Distributed under the Boost Software License 1.0 or (at your option)
 * the GNU General Public License 3.0 or later.
 * This file is offered as-is, without any warranty.
 */

module cmd.common;
import clibase : printLine, requestResponse, TT, CommonError, WrongUsage;

import std.file : mkdirRecurse;
import std.path : buildPath;

static import amalthea.fs;
import amalthea.fs : FileEntry, isDir;
import amalthea.langlocal : _s;
import amalthea.dataprocessing : stripRight, stripLeft, isAmong;

import configuration : getMountPoint;
static import filesystem;

alias W = TT.warning;
alias E = TT.error;
alias repNameEnding = filesystem.repeatedNameEnding;


/*******************************************************************************
 * The function creates category with the default response
 * to create new category if not exists.
 */
bool createNonExistentCategory(string category, bool yesMode = true) {
    string question = "Do you want to create this category? y/n >: "._s;
    if (yesMode || requestResponse(question)) {
        mkdirRecurse(category);
        return true;
    }
    return false;
}


string[] prepareCategories(string[] categories, bool doCreateCategories) {
    string[ulong] finalCategories;
    foreach(cat; categories) {
        FileEntry entry;
        if (!doesCategoryExist(cat)) {
            printLine!W(cat, ": this category doesn't exist."._s);
            if (!createNonExistentCategory(cat, doCreateCategories)) {
                printLine!W(cat, ": ignored."._s);
                continue;
            }
            entry = FileEntry(getVitisFileFullPath(cat));
        } else {
            string p = getVitisFileFullPath(cat);
            entry = FileEntry(p);
            if (!entry.isDir) {
                auto msg = p ~ ": this file is not category."._s;
                throw new WrongUsage(msg);
            }
            printLine!E(cat, ": the category is repeated.");
            if (cat.isAmong(finalCategories.values)) {
                printLine!W(cat, ": the category is repeated.");
                continue;
            }
        }
        if (entry.fileno in finalCategories) {
            printLine!E(cat, ": the category is repeated.");
            printLine!E(cat, "It is equal to "._s ~ cat);
        }
        finalCategories[entry.fileno] = entry.path;
    }
    return finalCategories.values;
}


string getVitisFileFullPath(string vitisPath) {
    vitisPath = vitisPath.stripLeft('/');
    return buildPath(getMountPoint(), vitisPath);
}


bool doesCategoryExist(string category) {
    category = stripLeft(category, '/');
    string p = getVitisFileFullPath(category);
    if (!amalthea.fs.exists(p)) {
        return false;
    }
    return amalthea.fs.isDir(p) || FileEntry(p).isLinkToDir();
}


ulong getDeviceOfFile(string file) {
    return amalthea.fs.FileStat(file).dev;
}


ulong getVitisDevice() {
    auto root = getMountPoint();
    return amalthea.fs.FileStat(root).dev;
}
