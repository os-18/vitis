/* This file is part of the Vitis project.
 *
 * Copyright (C) Eugene 'Vindex' Stulin, 2022-2024
 *
 * Distributed under the Boost Software License 1.0 or (at your option)
 * the GNU General Public License 3.0 or later.
 * This file is offered as-is, without any warranty.
 */

module cmd.show;

import configuration, filesystem, clibase, help;

import oxfuse;

import amalthea.fs;
import amalthea.dataprocessing :
    extractOptionValue,
    extractOptionRange,
    stripLeft,
    isAmong;
import amalthea.terminal : FGColor, cwrite, isTTY;
import amalthea.langlocal;
import amalthea.time;

import std.algorithm,
       std.array,
       std.file,
       std.getopt,
       std.path,
       std.process,
       std.string;


struct ShowOptions {
    bool categories;
    bool details;
    int number;
    SortEnum sortType;
}


enum SortEnum : string {
    name = "name",
    none = "none",
    size = "size",
    time = "time",
    atime = "atime",
    extension = "extension"
}


immutable wrongUsageMsg = "Incorrect use of 'vitis show'.";


void runShowCommand(string[] args) {
    if (args[2].isAmong("--help", "-h")) {
        enforce(args.length == 3, new WrongUsage(wrongUsageMsg._s));
        showHelp("show");
        return;
    }
    std.file.chdir(VitisConf.getMountPoint());
    if (!isOption(args[2])) {
        args = args[0 .. 2] ~ "-e" ~ args[2 .. $];
    }

    ShowOptions options;
    args.getopt(config.passThrough,
        "categories", &options.categories,
        "details",    &options.details,
        "number",     &options.number,
        "sort",       &options.sortType
    );

    switch(args[2]) {
        case "-d":
            string dir = extractOptionValue(args, "-d");
            showDirectoryContent(dir, options);
            break;
        case "-e":
            string[] expression = extractOptionRange(args, "-e");
            showFilesByExpression(expression, options);
            break;
        case "-c":
            string[] expression = extractOptionRange(args, "-c");
            if (expression.length > 1) {
                auto errMsg = "The '-c' option assumes only one value."._s;
                throw new WrongUsage([errMsg, wrongUsageMsg._s]);
            }
            showFilesByExpression(expression, options);
            break;
        default: throw new WrongUsage([wrongUsageMsg._s]);
    }
}


void showDirectoryContent(string dir, ShowOptions options) {
    dir = dir.stripLeft('/');
    string dirLocation = buildPath(VitisConf.getMountPoint(), dir);
    if (!std.file.exists(dirLocation)) {
        throw new CommonError(dir ~ ": this category doesn't exist."._s);
    }
    auto files = getFiles(dirLocation);
    size_t i = 1;
    foreach (f; files) {
        string name = !isDir(f) ? baseName(f.path) : baseName(f.path) ~ "/";
        printLine(name, i, files.length);
        if (options.categories) {
            printCategories(f.fileno.to!string);
        }
        if (options.details) {
            printDetails(f);
        }
        i++;
    }
}


private void printLine(string file,
                       ssize_t currNumber,
                       ssize_t maxNumber,
                       bool disableNumbers=false) {
    auto numberOfDigits = calcNumberOfDigits(maxNumber);
    if (!disableNumbers) {
        auto printableNumber = "[" ~ currNumber.to!string ~ "]";
        auto numberField = rightJustify(printableNumber, numberOfDigits+2);
        tprint(numberField, " ");
    }
    tprintln!(TT.filename)(file);
}


private size_t calcNumberOfDigits(long x) {
    long p = 10;
    for (size_t i = 1; i < 19; ++i) {
        if (x < p) {
        	return i;
        }
        p *= 10;
    }
    return 19;
}


void showFilesByExpression(string[] expression, ShowOptions options) {
    string expForFS = std.array.join(expression, "\n") ~ "\nxi";
    string errMsg = "The expression contains non-existent categories."._s;
    enforce!CommonError(std.file.exists(expForFS), errMsg);
    auto files = getFiles(expForFS);
    size_t i = 1;
    foreach (f; files) {
        string ino = f.fileno.to!string;
        auto name = filesystem.stripNodeId(baseName(f.path));
        printLine(name, i, files.length);
        if (options.categories) {
            printCategories(ino);
        }
        if (options.details) {
            printDetails(f);
        }
        i++;
    }
}


private void printCategories(string fileId) {
    /* getCategoryListByInodeNumber(fileId) */
    /*     .sort */
    /*     .map!framing */
    /*     .join(" ") */
    /*     .tprintln!(TT.category); */
}


string framing(string s, string frame=`"`) {
    return frame ~ s.replace(frame, "\\"~frame) ~ frame;
}


string extractInoNumber(string fileNameWithId) {
    return fileNameWithId.split(":")[1][2 .. $];
}


/* string[] getCategoryListByInodeNumber(string ino) { */
/*     string req = ino ~ SpecialSuffix.categoriesByIno; */
/*     string[] catInoNumbers = std.file.readText(req).splitLines; */
/*     string[] categories; */
/*     foreach(catIno; catInoNumbers) { */
/*         string reqForCatName = catIno ~ SpecialSuffix.categoryNameByIno; */
/*         categories ~= std.file.readText(reqForCatName); */
/*     } */
/*     return categories; */
/* } */


private void printDetails(FileEntry entry) {
    FileStat fstat = entry.getFileStat();
    string lastModification = amalthea.time.getTimeString(fstat.mtime);
    string lastAccess = amalthea.time.getTimeString(fstat.atime);
    string permissions = amalthea.fs.makeUnixFileModeLine(fstat.mode);
    string normSize = makeHumanOrientedByteSize(fstat.size);
    if (fstat.size >= 1024) {
        normSize = format!"%s (%s %s)"(normSize, fstat.size, "B"._s);
    }

    tprint!(TT.propertyName)("Modified: "._s);
    tprint!(TT.propertyValue)(lastModification, "\t");
    tprint!(TT.propertyName)("Permissions: "._s);
    tprintln!(TT.propertyValue)(permissions[1 .. $]);

    tprint!(TT.propertyName)("Accessed: "._s);
    tprint!(TT.propertyValue)(lastAccess, "\t");
    tprint!(TT.propertyName)("Size: "._s);
    tprintln!(TT.propertyValue)(normSize);
}


private long getExponent(ulong x) {
    if (x == 0) {
        return 0;
    }
    long exp = -1;
    while(x != 0) {
        x = x >> 10;
        ++exp;
    }
    return exp;
}


private string makeHumanOrientedByteSize(ulong bytes) {
    auto exponent = getExponent(bytes);
    if (exponent > 5) exponent = 5;
    string unit = exponent.predSwitch(
        0, "B"._s,
        1, "KiB"._s,
        2, "MiB"._s,
        3, "GiB"._s,
        4, "TiB"._s,
        5, "PiB"._s,
        null
    );
    real size = to!real(bytes)/(1024^^exponent);
    auto strSize = exponent == 0 ? to!string(size) : format!"%.2f"(size);
    return strSize ~ " " ~ unit;
}
unittest {
    assert(makeHumanOrientedByteSize(0) == "0 B");
    assert(makeHumanOrientedByteSize(1023) == "1023 B");
    assert(makeHumanOrientedByteSize(1024) == "1.00 KiB");
    assert(makeHumanOrientedByteSize(1025) == "1.00 KiB");
    assert(makeHumanOrientedByteSize(1024*1024*512) == "512.00 MiB");
    assert(makeHumanOrientedByteSize(1024L^^3*5/4) == "1.25 GiB");
    assert(makeHumanOrientedByteSize(1024L^^3*7/4) == "1.75 GiB");
}

