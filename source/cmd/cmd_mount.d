/* This file is part of the Vitis project.
 *
 * Copyright (C) Eugene 'Vindex' Stulin, 2022-2025
 *
 * Distributed under
 * the Boost Software License 1.0 or (at your option)
 * the GNU General Public License 3.0 or later.
 * This file is offered as-is, without any warranty.
 */

module cmd.cmd_mount;

static import core.stdc.stdlib;

static import std.file;
static import std.process;
static import std.traits;
import std.array : empty, split;
import std.algorithm : endsWith, startsWith;
import std.conv : oct = octal, to;
import std.exception : collectException, enforce;
import std.file : dirEntries, SpanMode;
import std.format : format;
import std.path : baseName, buildPath;
import std.stdio : File, writefln, stderr;

static import amalthea.fs;
static import amalthea.sys;
import amalthea.dataprocessing : extractOptionValue, isAmong, stripLeft;
import amalthea.fs : exists, getAbsolutePath, DirReader, FileEntry, FileType;
import amalthea.fs : isSymlink;
import amalthea.langlocal : _s;
import amalthea.sys : loopctl;
import amalthea.sys : addSignalHandler, Signal, getSignalDescription;

static import oxfuse;

import clibase : WrongUsage, CommonError;
import filesystem : Vitis;
import configuration : getVitisMountPoints, sysDir;


enum ulong EXT4_FLAGS = amalthea.sys.MS_STRICTATIME;
enum uint DEFAULT_LOOP_NO = 81;


immutable mountWrongMsg = "Incorrect use of 'vitis mount'.";
immutable mountSeeHelpMsg = "See: vitis mount --help";
immutable umountWrongMsg = "Incorrect use of 'vitis umount'.";
immutable umountSeeHelpMsg = "See: vitis umount --help";
string[] mountErrMsgs;
string[] umountErrMsgs;

shared static this() {
    mountErrMsgs = [mountWrongMsg._s, mountSeeHelpMsg._s];
    umountErrMsgs = [umountWrongMsg._s, umountSeeHelpMsg._s];
}


string savedMountPoint;


void onSignal(Signal signal) {
    writefln("\nHandling signal %s...", signal);
    writefln("Event: %s", getSignalDescription(signal));
    try {
        umountByMountPoint(savedMountPoint);
    } catch (Exception e) {
        stderr.writeln(e.msg);
        return;
    }
    core.stdc.stdlib.exit(1);
}


void runMountCommand(string[] args) {
    string fsFile = extractOptionValue(args, "-i");
    enforce(!fsFile.empty, new WrongUsage(mountErrMsgs));
    string mountPoint = extractOptionValue(args, "-m");
    mountVitis(fsFile, mountPoint, args[2 .. $]);
}


string makeSecondaryMountPoint(string primaryMountPoint) {
    return primaryMountPoint ~ "-vitis";
}


void mountVitis(string fsFile, string mountPoint, string[] fuseArgs) {
    string[] loopDevices = loopctl.findDevicesByFile(fsFile);
    if (loopDevices.length > 0) {
        auto msg = "This file already associated with some loop device."._s;
        msg ~= "\n" ~ "Check: losetup -l"._s;
        throw new CommonError(msg);
    }
    uint no = chooseSuitableLoopDevice(fsFile);
    string loopName = format!"loop%u"(no);
    string loopPath = buildPath("/dev", loopName);
    if (!exists(loopPath)) {
        loopctl.addLoopDevice(no);
    }
    loopctl.associateFileWithDevice(fsFile, no);

    // like /etc/vitis/loop81
    string primaryMountPoint = buildPath(sysDir, loopName);
    if (!exists(primaryMountPoint)) {
        std.file.mkdirRecurse(primaryMountPoint);
    }
    string secondaryPath = makeSecondaryMountPoint(primaryMountPoint);
    if (mountPoint.empty) {
        mountPoint = secondaryPath;
        if (!amalthea.fs.exists(mountPoint)) {
            std.file.mkdirRecurse(mountPoint);
        }
    } else if (!amalthea.fs.exists(mountPoint)) {
        throw new CommonError("Mount point does not exist."._s);
    } else {
        if (exists(secondaryPath)) {
            std.file.remove(secondaryPath);
        }
        std.file.symlink(mountPoint, secondaryPath);
    }
    amalthea.sys.mount(loopPath, primaryMountPoint, "ext4", EXT4_FLAGS);
    scope(failure) amalthea.sys.umount(primaryMountPoint);
    savedMountPoint = mountPoint;
    foreach (sig; [Signal.SIGINT, Signal.SIGTERM, Signal.SIGQUIT]) {
        addSignalHandler(sig, &onSignal);
    }
    auto fs = new Vitis(fsFile, primaryMountPoint);
    auto code = oxfuse.mount("Vitis", fs, mountPoint, fuseArgs);
    enforce!CommonError(0 == code, format!"FUSE error: %d."(code));
}


void runUmountCommand(string[] args) {
    try {
        if ("-m".isAmong(args)) {
            string mountPoint = extractOptionValue(args, "-m");
            enforce(args.length == 2, new WrongUsage(umountErrMsgs));
            mountPoint = getAbsolutePath(mountPoint);
            umountByMountPoint(mountPoint);
        } else if ("-i".isAmong(args)) {
            string fsFile = extractOptionValue(args, "-i");
            fsFile = getAbsolutePath(fsFile);
            enforce(!fsFile.empty, new WrongUsage(umountErrMsgs));
            enforce(args.length == 2, new WrongUsage(umountErrMsgs));
            umountByImage(fsFile);
        }
    } catch (Exception e) {
        stderr.writeln(e.msg);
    }
}


void umountByMountPoint(string mountPoint) {
    string[string] allMountPoints = getVitisMountPoints();
    string primaryMountPoint;
    foreach(primaryMP, vitisMP; allMountPoints) {
        if (vitisMP == mountPoint) {
            primaryMountPoint = primaryMP;
            break;
        }
    }
    if (primaryMountPoint.empty) {
        string msg = "You're trying to unmount something unexpected.";
        throw new CommonError(msg._s);
    }
    // primaryMountPoint is equal to /etc/vitis/loopXX
    string loop = buildPath("/dev", baseName(primaryMountPoint));
    uint loopNo = loop.stripLeft("/dev/loop").to!uint;
    string fsFile = loopctl.getBackingFile(loopNo);
    enforce(!mountPoint.empty, new WrongUsage(umountErrMsgs));
    umountVitis(fsFile, mountPoint, primaryMountPoint, loop);
}


void umountByImage(string fsFile) {
    string[] loops = loopctl.findDevicesByFile(fsFile);
    enforce!CommonError(!loops.empty, "Related loop device not found."._s);
    string loop = loops[0];
    string[] points = amalthea.sys.findMountPoints(loop);
    if (points.empty) {
        throw new CommonError("This file system is not mounted."._s);
    }
    string primaryMountPoint = points[0];
    string[string] allMountPoints = getVitisMountPoints();
    string mountPoint = allMountPoints[primaryMountPoint];
    enforce(!mountPoint.empty, new WrongUsage(umountErrMsgs));
    umountVitis(fsFile, mountPoint, primaryMountPoint, loop);
}


void umountVitis(
    string fsImage,
    string mountPoint,
    string primaryMountPoint,
    string loopDevice
) {
    amalthea.sys.umount(mountPoint);
    amalthea.sys.umount(primaryMountPoint);
    std.file.remove(primaryMountPoint);
    std.file.remove(makeSecondaryMountPoint(primaryMountPoint));
    loopctl.disassociateFileWithDevice(fsImage, loopDevice);
}


private uint chooseSuitableLoopDevice(string fsFile) {
    uint loopIno = DEFAULT_LOOP_NO;
    fsFile = getAbsolutePath(fsFile);
    string[] loops = loopctl.findDevicesByFile(fsFile);
    if (!loops.empty) {
        loopIno = loops[0].stripLeft("/dev/loop").to!uint;
    }
    increaseWhile!(a => loopctl.doesLoopDeviceExistAndIsBusy(a))(loopIno);
    return loopIno;
}


private void increaseWhile(alias pred, T)(ref T value)
if (std.traits.isNumeric!T)
do {
    while(pred(value)) {
        value++;
    }
}
