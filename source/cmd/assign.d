/* This file is part of the Vitis project.
 *
 * Copyright (C) Eugene 'Vindex' Stulin, 2022-2024
 *
 * Distributed under the Boost Software License 1.0 or (at your option)
 * the GNU General Public License 3.0 or later.
 * This file is offered as-is, without any warranty.
 */

module cmd.assign;

import configuration, filesystem, clibase, help;

import oxfuse;

import amalthea.fs;
import amalthea.dataprocessing :
    extractOptionValue,
    extractOptionRange,
    stripLeft,
    isAmong;
import amalthea.terminal : FGColor, cwrite, isTTY;
import amalthea.langlocal;
import amalthea.time;

import std.algorithm,
       std.array,
       std.file,
       std.getopt,
       std.path,
       std.process,
       std.string;


struct AssignOptions {
    bool yesMode;
    bool all;
}


immutable wrongMsg = "Incorrect use of 'vitis assign'.";
immutable seeHelpMsg = "See: vitis assign --help";

alias W = TT.warning;


void runAssignCommand(string[] args) {
    auto errMessages = [wrongMsg._s, seeHelpMsg._s];
    if (args[2].isAmong("--help", "-h")) {
        enforce(args.length == 3, new WrongUsage(errMessages));
        showHelp("assign");
        return;
    }
    std.file.chdir(VitisConf.getMountPoint());

    AssignOptions options;
    args.getopt(
        config.passThrough,
        "all", &options.all,
        "yes", &options.yesMode
    );

    string[] categories = extractOptionRange(args, "-c");
    string[] aliases = extractOptionRange(args, "-a");
    string[] files = extractOptionRange(args, "-f");
    string[] vitisFiles = extractOptionRange(args, "-v");
    string[] expression = extractOptionRange(args, "-e");
    string newName = extractOptionValue(args, "-n");
    string newCanonName = extractOptionValue(args, "-N");

    // vitis assign -c <category> -a <alias>
    if (!aliases.empty) {
        enforce(categories.length == 1, new WrongUsage(errMessages));
        createAliases(categories[0], aliases, options.yesMode);
        return;
    }
    // vitis assign -f <file> ... [-c <category> ...] [-I]
    if (!files.empty) {
        addFilesToVitis(files, categories, options.yesMode);
        return;
    }
    // vitis assign -c <category> -n <new name>
    if (!newName.empty) {
        enforce(categories.length == 1, new WrongUsage(errMessages));
        renameCategory(categories[0], newName);
    }
    // vitis assign -e <expression> -c <category> ...
    if (!expression.empty) {
        addFilesByExpression(expression, categories);
    }
    // vitis assign -v <category/file> ... -c <new_category> ...
    if (!vitisFiles.empty) {
        auto errPath = ": file path cannot start with a slash";
        foreach (f; vitisFiles) {
            if (f.startsWith('/')) {
                throw new WrongUsage([f ~ errPath._s, seeHelpMsg._s]);
            }
        }
        addFilesToVitis(vitisFiles, categories, options.yesMode);
        return;
    }
}


/*******************************************************************************
 * The function creates category with the default response
 * to create new category if not exists.
 */
private bool createNonExistentCategory(string category, bool yesMode) {
    string answer = "Do you want to create this category? y/n >: "._s;
    if (yesMode || requestResponse(answer)) {
        mkdirRecurse(category);
        return true;
    }
    return false;
}


/*******************************************************************************
 * The function creates aliases for some category.
 * This is used for 'vitis assign -c <category> -a <alias>...'.
 */
void createAliases(string category, string[] aliases, bool yesMode) {
    if (!doesCategoryExist(category)) {
        string errMsg = category ~ ": file exists and it is not category."._s;
        enforce!CommonError(!doesFileInVitisExist(category), errMsg);
        tprintln!(TT.warning)(category, ": this category doesn't exist."._s);
        bool created = createNonExistentCategory(category, yesMode);
        if (!created) {
            return;
        }
    }
    string request = "\n" ~ category;
    foreach(catAlias; aliases) {
        if (amalthea.fs.exists(catAlias)) {
            tprintln!(TT.warning)(catAlias, ": such file exists."._s);
            continue;
        }
        std.file.symlink(request, catAlias);
    }
}


/*******************************************************************************
 * The function assigns categories to files.
 * This is used for 'vitis assign -c <category> ... -f <file>...'.
 */
void addFilesToVitis(string[] files, string[] categories, bool yesMode) {
    categories = filterCategories(categories, yesMode);
    files = filterFiles(files);
    foreach(cat; categories) {
        foreach(ref f; files) {
            string path = buildPath(cat, std.path.baseName(f));  // new path
            if (amalthea.fs.exists(path)) {
                tprintln!W(path, ": such file exists."._s, " ", "Skipped."._s);
                continue;
            }
            if (getDeviceOfFile(f) != getVitisDevice()) {
                amalthea.fs.cp(f, path);
                f = path;
            } else {
                amalthea.fs.hardlink(f, path);
            }
        }
    }
}


/*******************************************************************************
 * The function assigns categories to files by category set expression.
 * It corresponds to 'vitis assign -c <category> ... -e <expression>'.
 */
void addFilesByExpression(string[] expression, string[] categories) {
    string expForFS = std.array.join(expression, "\n") ~ "\nxi";
    string errMsg = "The expression contains non-existent categories."._s;
    enforce!CommonError(std.file.exists(expForFS), errMsg);
    foreach (f; getFiles(expForFS)) {
        string fileName = std.path.baseName(f.path);
        foreach (cat; categories) {
            amalthea.fs.hardlink(f.path, buildPath(cat, fileName));
        }
    }
}


immutable string dirNotAllowed =
    ": assigning directories to categories is not allowed.";


string[] filterCategories(string[] categories, bool yesMode) {
    string[] finalCategories;
    foreach(cat; categories) {
        if (!doesCategoryExist(cat)) {
            tprintln!W(cat, ": this category doesn't exist."._s);
            if (!createNonExistentCategory(cat, yesMode)) {
                tprintln!W(cat, ": ignored."._s);
                continue;
            }
        } else if (cat.isAmong(finalCategories)) {
            tprintln!W(cat, ": the category is repeated.");
            continue;
        } else if (FileEntry(cat).isLinkToDir()) {
            string targ = readLink(cat);
            if (targ.isAmong(finalCategories)) {
                tprintln!W(cat, "(", targ, ")", ": the category is repeated.");
                continue;
            }
            cat = targ;
        }
        finalCategories ~= cat;
    }
    return finalCategories;
}


string[] filterFiles(string[] files) {
    string[ulong] finalFiles;
    foreach(f; files) {
        if (!amalthea.fs.exists(f)) {
            tprintln!W(f, ": file not found."._s, " ", "Skipped."._s);
            continue;
        } else if (amalthea.fs.isDir(f)) {
            tprintln!W(f, dirNotAllowed._s, " ", "Skipped."._s);
            continue;
        }
        ulong ino = getFileNo(f);
        if (ino in finalFiles) {
            auto twoFiles = finalFiles[ino] ~ " and "._s  ~ f;
            tprintln!W(twoFiles, " are the same file."._s);
            tprintln!W("The duplicate file is ignored."._s);
            continue;
        }
        finalFiles[ino] = f;
    }
    return finalFiles.values;
}


ulong getFileNo(string filepath) {
    return FileEntry(filepath).fileno;
}


void renameCategory(string oldName, string newName) {
    std.file.rename(oldName, newName);
}



bool doesCategoryExist(string category) {
    category = stripLeft(category, '/');
    string p = getAbsolutePath(buildPath(VitisConf.getMountPoint(), category));
    if (!amalthea.fs.exists(p)) {
        return false;
    }
    if (getDeviceOfFile(p) != getVitisDevice()) {
        return false;
    }
    return amalthea.fs.isDir(p) || FileEntry(p).isLinkToDir();
}


bool doesFileInVitisExist(string fileInVitis) {
    fileInVitis = stripLeft(fileInVitis, '/');
    auto p = getAbsolutePath(buildPath(VitisConf.getMountPoint(), fileInVitis));
    return amalthea.fs.exists(p) && getDeviceOfFile(p) == getVitisDevice();
}



ulong getDeviceOfFile(string file) {
    return amalthea.fs.FileStat(file).dev;
}


ulong getVitisDevice() {
    auto root = VitisConf.getMountPoint();
    return amalthea.fs.FileStat(root).dev;
}
