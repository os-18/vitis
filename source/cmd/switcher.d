/* This file is part of the Vitis project.
 *
 * Copyright (C) Eugene 'Vindex' Stulin, 2022-2024
 *
 * Distributed under the Boost Software License 1.0 or (at your option)
 * the GNU General Public License 3.0 or later.
 * This file is offered as-is, without any warranty.
 */

module cmd.switcher;

import std.exception;

import amalthea.langlocal;

import clibase, configuration;


immutable wrongMsg = "Incorrect use of 'vitis switch'.";
immutable seeHelpMsg = "See: vitis switch --help";


void runSwitchCommand(string[] args) {
    args = args[2 .. $];
    enforce(args.length == 1, new WrongUsage([wrongMsg._s, seeHelpMsg._s]));
    string image = args[0];
    switchImage(image);
}


void switchImage(string image) {
    VitisConf.setCurrentFile(image);
}
