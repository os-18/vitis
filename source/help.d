/* This file is part of the Vitis project.
 *
 * Copyright (C) Eugene 'Vindex' Stulin, 2021-2022
 *
 * Distributed under the Boost Software License 1.0 or (at your option)
 * the GNU General Public License 3.0 or later.
 * This file is offered as-is, without any warranty.
 */

import std.array, std.file, std.path, std.string;
import amalthea.langlocal, amalthea.sys;

import configuration;

private string getHelpDir() {
    string lang = VitisConf.getLanguage();
    if (lang == "auto") {
        lang = amalthea.langlocal.getSystemLanguage();
    }
    string baseHelpDir = amalthea.sys.getAppHelpDir();
    string vitisHelpDir = buildPath(baseHelpDir, lang, "vitis");
    if (!exists(vitisHelpDir)) {
        vitisHelpDir = buildPath(baseHelpDir, "en_US", "vitis");
    }
    if (!exists(vitisHelpDir)) {
        throw new FileException("Help file does not exist."._s);
    }
    return vitisHelpDir;
}


/// Prints help information.
void showHelp(string command = "") {
    string dir = getHelpDir();
    string txtFile = command.empty ? buildPath(dir, "help_begin.txt")
                                   : buildPath(dir, "help_" ~ command ~ ".txt");
    writeln(readText(txtFile).strip);
}
