This program is distributed in the hope that it will be useful,
but without any warranty; without even the implied warranty of
merchantability or fitness for a particular purpose.

License: Boost Software License 1.0 or GNU GPL v3+.

---

## vitis

Project is under development.  
Expected time to release the first working version: Spring 2025.


Old project: [vitis-sl 0.13.x](https://gitlab.com/tech.vindex/vitis-sl)

---

## Feedback

Questions, suggestions, comments, bugs:

**tech.vindex@gmail.com**

Also use the repository service tools.

---

(c) Eugene 'Vindex' Stulin, 2021-2025
