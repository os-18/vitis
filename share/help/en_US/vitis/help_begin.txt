Vitis is a semantic file system and category manager for files.
The vitis settings are stored in the file '~/.config/vitis/vitis.conf'.

As first argument, vitis takes one of following commands:
    assign    — assignment of categories for files and some other operations;
    create    — creation of new categories;
    delete    — deletion of categories and deleting of links from categories;
    mount     — allows to mount Vitis file system;
    open      — allows open files;
    show      — output of lists of files and categories on the screen;
    umount    — allow to unmount Vitis file system.
Additional information:
    --help    — displaying of this help;
    --version — displaying of version of the program.

For each command, you can add '--help' (or '-h') to display help about command.
Example: 'vitis create --help'.

The option '--conf=<path_to_configuration_file> is applied to any
command, it allows to use an alternative configuration file.

See the help of specific commands for more information:
    vitis assign --help
    vitis create --help
    vitis delete --help
    vitis open --help
    vitis show --help

